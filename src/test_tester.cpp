
#include <cstdint>
#include <ios>
#include <iostream>
#include <list>
#include <iomanip>

// Boost
#include <boost/thread/mutex.hpp>

// ROS
#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

std::uint32_t k = 3;

ros::Publisher pub_sync;

boost::mutex mMutexTimestamps;
std::list<double> timestamps;

void ImageCallback(const sensor_msgs::ImagePtr& msg) {
  k++;
  
  if ((k % 50) == 0) {
    std_msgs::Header sync_data;
    
    sync_data.seq = k;
    sync_data.stamp = msg->header.stamp;
   
   /* 
    {
      boost::mutex::scoped_lock lock(mMutexTimestamps);
      timestamps.push_back(msg->header.stamp.toSec());
    }
    */
    
    pub_sync.publish(sync_data);
  }
}

void ImuCallback(const geometry_msgs::PoseWithCovarianceStampedPtr& msg) {
  double timestamp = msg->header.stamp.toSec();
  
  /*
  {
    boost::mutex::scoped_lock lock(mMutexTimestamps);
    timestamp = timestamps.front();
    timestamps.pop_front();
  }
  */
  
  std::cout << std::fixed;
  
  std::cout << std::setprecision(6)
            << timestamp << " ";
            
  std::cout << std::setprecision(7)
            << msg->pose.pose.position.x << " "
            << msg->pose.pose.position.y << " "
            << msg->pose.pose.position.z << " "
            << msg->pose.pose.orientation.x << " "
            << msg->pose.pose.orientation.y << " "
            << msg->pose.pose.orientation.z << " "
            << msg->pose.pose.orientation.w << " "
            << std::endl;
}

int main(int argc, char* argv[]) {
  
  ros::init(argc, argv, "test_tester");
  
  ros::NodeHandle nodeHandler;
  
  ros::Subscriber sub_img = nodeHandler.subscribe("/camera/image_raw", 100, &ImageCallback);
  ros::Subscriber sub_imu = nodeHandler.subscribe("/imu/estimate", 100, &ImuCallback);
  
  pub_sync = nodeHandler.advertise<std_msgs::Header>("/orb_vo/sync", 100);
  
  ros::spin();
  
  return 0;
}
