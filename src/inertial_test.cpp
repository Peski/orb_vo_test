
#define PROGRAM_NAME \
    "inertial_test"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_origin, 0.0, "New time origin [s]")

#define ARGS_CASES                                                                                 \
    ARG_CASE(trajectory_file)                                                                      \

#include <algorithm>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <string>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// ROS
#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <geographic_msgs/GeoPoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include "csv.hpp"

#include "gps.hpp"
#include "SE3Quat.h"

#include "quat.hpp"
#include "jacobians.hpp"

#include "util/macros.h"

namespace fs = boost::filesystem;

bool initialized = false;
std::vector<double> timestamps;
std::vector<geographic_msgs::GeoPose> geo_poses;

ros::Publisher pub_imu;

std::int64_t src_idx = -1, dst_idx = -1;

inline double RadToDeg(const double rad) {
  return rad * 57.29577951308232286464772187173366546630859375;
}

inline void ValidateArgs() {
  RUNTIME_ASSERT(fs::is_regular_file(ARGS_trajectory_file));
}

inline void ValidateFlags() {
  RUNTIME_ASSERT(std::isfinite(FLAGS_time_origin));
}

std::int64_t find_closest(const std::vector<double>& v, double value) {
  if (v.size() < 2) return -1;
  
  std::vector<double>::const_iterator nxt = std::lower_bound(v.cbegin(), v.cend(), value);
  if (nxt == v.cbegin() || nxt == v.cend()) return -1;
  
  std::vector<double>::const_iterator prv = std::prev(nxt);
  if ((value - *prv) < (*nxt - value)) {
    return std::distance(v.cbegin(), prv);
  } else {
    return std::distance(v.cbegin(), nxt);
  }
}

std::string to_string(double number) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(6) << number;
  return ss.str();
}

void SyncCallback(const std_msgs::HeaderPtr& msg) {

  src_idx = dst_idx;
  dst_idx = find_closest(timestamps, msg->stamp.toSec());
  
  if (!initialized) {
    initialized = true;
    return;
  }
  
  if (src_idx == -1 || dst_idx == -1) {
    std::cerr << "[SyncCallback] Invalid index!" << std::endl;
    return;
  }
  
  geographic_msgs::GeoPose src_geo = geo_poses.at(src_idx);
  geographic_msgs::GeoPose dst_geo = geo_poses.at(dst_idx);
  
  Eigen::Isometry3d src_ecef = Eigen::Isometry3d::Identity();
  src_ecef.translation() = gps::geodetic2Ecef(src_geo.position.latitude, src_geo.position.longitude, src_geo.position.altitude);
  src_ecef.linear() = gps::nRe(src_geo.position.latitude, src_geo.position.longitude).transpose() * Eigen::Quaterniond(src_geo.orientation.w, src_geo.orientation.x, src_geo.orientation.y, src_geo.orientation.z).toRotationMatrix();
  
  Eigen::Isometry3d dst_ecef = Eigen::Isometry3d::Identity();
  dst_ecef.translation() = gps::geodetic2Ecef(dst_geo.position.latitude, dst_geo.position.longitude, dst_geo.position.altitude);
  dst_ecef.linear() = gps::nRe(dst_geo.position.latitude, dst_geo.position.longitude).transpose() * Eigen::Quaterniond(dst_geo.orientation.w, dst_geo.orientation.x, dst_geo.orientation.y, dst_geo.orientation.z).toRotationMatrix();
  
  SE3Quat relative_pose(src_ecef.inverse() * dst_ecef);
  
  Eigen::MatrixXd covariance(6, 6);
  covariance.setZero();
  
  covariance(0, 0) = 1.0;
  covariance(1, 1) = 1.0;
  covariance(2, 2) = 1.0;
  covariance(3, 3) = 0.1;
  covariance(4, 4) = 0.1;
  covariance(5, 5) = 0.1;
  
  geometry_msgs::PoseWithCovarianceStamped imu_data;
  
  imu_data.header.stamp = msg->stamp;
  
  imu_data.pose.pose.position.x = relative_pose.t[0];
  imu_data.pose.pose.position.y = relative_pose.t[1];
  imu_data.pose.pose.position.z = relative_pose.t[2];
  
  imu_data.pose.pose.orientation.x = relative_pose.q[1];
  imu_data.pose.pose.orientation.y = relative_pose.q[2];
  imu_data.pose.pose.orientation.z = relative_pose.q[3];
  imu_data.pose.pose.orientation.w = relative_pose.q[0];
  
  for (int i = 0; i < 6; ++i)
    for (int j = 0; j < 6; ++j)
      imu_data.pose.covariance[i*6 + j] = covariance(i, j);
  
  std::cout << to_string(msg->stamp.toSec()) << " "
            << to_string(relative_pose.t[0]) << " "
            << to_string(relative_pose.t[1]) << " "
            << to_string(relative_pose.t[2]) << " "
            << to_string(relative_pose.q[1]) << " "
            << to_string(relative_pose.q[2]) << " "
            << to_string(relative_pose.q[3]) << " "
            << to_string(relative_pose.q[0]) << " "
            << std::endl;
  
  pub_imu.publish(imu_data);
}

int main(int argc, char* argv[]) {

  ros::init(argc, argv, "orb_vo_test");
  
  // Handle help flag
  if (args::HelpRequired(argc, argv)) {
    args::ShowHelp();
    return 0;
  }

  // Parse input flags
  args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

  // Check number of args
  if (argc-1 != args::NumArgs()) {
    args::ShowHelp();
    return -1;
  }

  // Parse input args
  args::ParseCommandLineArgs(argc, argv);

  // Validate input arguments
  ValidateFlags();
  ValidateArgs();
  
  // Read file
  std::cout << "Reading file " << ARGS_trajectory_file << std::endl;
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> data =
      csv::read<double, Eigen::RowMajor>(ARGS_trajectory_file, ' ');
  RUNTIME_ASSERT(data.cols() == 8); // 8 columns expected
  RUNTIME_ASSERT(data.rows() > 0); // invalid file?
  
  // Sort by time offset
  //  (we sort row pointers to avoid copying the whole matrix)
  std::vector<double*> data_rowwise;
  data_rowwise.reserve(data.rows());
  for (int i = 0; i < data.rows(); ++i)
    data_rowwise.push_back(data.data() + 8*i);

  std::sort(data_rowwise.begin(), data_rowwise.end(),
    [](double* lhs, double* rhs) {
        return lhs[0] < rhs[0];
    });
  
  for (double *row_data : data_rowwise) {
  
    const double timestamp = FLAGS_time_origin + row_data[0];
    timestamps.push_back(timestamp);
    
    geographic_msgs::GeoPose geo_pose;
    
    // Pose
    //  Position
    geo_pose.position.latitude = RadToDeg(row_data[1]); // deg
    geo_pose.position.longitude = RadToDeg(row_data[2]); // deg
    geo_pose.position.altitude = row_data[3]; // m
        
    //  Orientation
    geo_pose.orientation.x = row_data[5];
    geo_pose.orientation.y = row_data[6];
    geo_pose.orientation.z = row_data[7];
    geo_pose.orientation.w = row_data[4];
    
    geo_poses.push_back(geo_pose);
  }
  
  std::cout << "done." << std::endl;
  
  ros::NodeHandle nodeHandler;
  
  ros::Subscriber sub_sync = nodeHandler.subscribe("/orb_vo/sync", 100, &SyncCallback);
  
  pub_imu = nodeHandler.advertise<geometry_msgs::PoseWithCovarianceStamped>("/imu/estimate", 100);
  
  ros::spin();
  
  return 0;
}
