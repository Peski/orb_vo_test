
// STL
#include <chrono>
#include <cmath>
#include <iomanip>
#include <ios>
#include <iostream>
#include <random>
#include <vector>

// Eigen
#include <Eigen/Core>

#include "quat.hpp"
#include "jacobians.hpp"
#include "SE3Quat.h"

const double K = EIGEN_PI / 180.0;

std::mt19937* PRNG;


double RandomGaussian(const double mean, const double stddev) {
  std::normal_distribution<double> distribution(mean, stddev);
  return distribution(*PRNG);
}

double RandomReal(const double min, const double max) {
  std::uniform_real_distribution<double> distribution(min, max);
  return distribution(*PRNG);
}

int main() {

  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
  PRNG = new std::mt19937(seed);

  std::cout << std::fixed << std::setprecision(6);
  std::cout << "Jacobian test" << std::endl;
  
  const double K = 10.0;
  
  double t[3] = {RandomGaussian(0.0, K*0.1), RandomGaussian(0.0, K*0.1), RandomGaussian(0.0, K*0.1)};
  double ypr[3] = {RandomGaussian(0.0, K*0.5), RandomGaussian(0.0, K*0.5), RandomGaussian(0.0, K*0.5)};
  
  const double C = EIGEN_PI / 180.0;
  
  Eigen::MatrixXd covariance(6, 6);
  covariance.setZero();
  
  covariance.block<3, 3>(0, 0) = std::pow(K*0.1, 2)*Eigen::Matrix3d::Identity();
  covariance.block<3, 3>(3, 3) = std::pow(K*0.5, 2)*Eigen::Matrix3d::Identity();
  
  ypr[0] *= C;
  ypr[1] *= C;
  ypr[2] *= C;
  
  Eigen::MatrixXd J;
  J.resize(6, 6);
  J.setZero();
  
  J.block<3, 3>(0, 0) =     Eigen::Matrix3d::Identity();
  J.block<3, 3>(3, 3) = C * Eigen::Matrix3d::Identity();
  
  covariance = J*covariance*J.transpose();
  
  double q[4];
  fromYPR(ypr, q);
  
  SE3Quat e;
  
  e.q[0] = q[0];
  e.q[1] = q[1];
  e.q[2] = q[2];
  e.q[3] = q[3];
  
  e.t[0] = t[0];
  e.t[1] = t[1];
  e.t[2] = t[2];
  
  J.resize(7, 6);
  J.setZero();
  
  J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  fromYPRJacobian(ypr, J.block<4, 3>(3, 3));
  
  covariance = J*covariance*J.transpose();
  
  double p_ypr[3] = {RandomReal(-180.0, 180.0), RandomReal(-90.0, 90.0), RandomReal(-180.0, 180.0)};
  double p_t[3] = {RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0)};
  
  SE3Quat p;
  
  p_ypr[0] *= C;
  p_ypr[1] *= C;
  p_ypr[2] *= C;
  
  double p_q[4];
  fromYPR(p_ypr, p_q);
  
  p.q[0] = p_q[0];
  p.q[1] = p_q[1];
  p.q[2] = p_q[2];
  p.q[3] = p_q[3];
  
  p.t[0] = p_t[0];
  p.t[1] = p_t[1];
  p.t[2] = p_t[2];  
  
  Eigen::MatrixXd J_p(7, 7);
  
  J.resize(7, 7);
  compositionJacobians(p, e, J_p, J);
  
  covariance = J*covariance*J.transpose();
  
  SE3Quat result = p*e;
  
  J.resize(6, 7);
  J.setZero();
  
  J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  toYPRJacobian(result.q, J.block<3, 4>(3, 3));
  
  covariance = J*covariance*J.transpose();
  
  std::cout << "Covariance: (" << covariance.rows() << ", " << covariance.cols() << ")" << std::endl;
  std::cout << covariance << std::endl;
  std::cout << std::endl;
  
  /*  
  double ypr[3] = {RandomReal(-180.0, 180.0), RandomReal(-90.0, 90.0), RandomReal(-180.0, 180.0)};
  double t[3] = {RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0)};
  
  ypr[0] *= K; ypr[1] *= K; ypr[2] *= K;
  
  double q[4];
  fromYPR(ypr, q);
  
  double q_inv[4];
  qinverse(q, q_inv);
  
  double t_inv[3];
  qrotate(q_inv, t, t_inv);
  t_inv[0] *= -1.0;
  t_inv[1] *= -1.0;
  t_inv[2] *= -1.0;
  
  double q_[4];
  qinverse(q_inv, q_);
  
  double t_[3];
  qrotate(q_, t_inv, t_);
  t_[0] *= -1.0;
  t_[1] *= -1.0;
  t_[2] *= -1.0;
  
  double ypr_[3];
  toYPR(q_, ypr_);
  
  //ypr_[0] /= K; ypr_[1] /= K; ypr_[2] /= K;
  
  std::cout << std::fixed << std::setprecision(6);
  
  std::cout << "y: " << ypr[0] << " p: " << ypr[1] << " r: " << ypr[2] << std::endl;
  std::cout << "y: " << ypr_[0] << " p: " << ypr_[1] << " r: " << ypr_[2] << std::endl;
  
  std::cout << "tx: " << t[0] << " ty: " << t[1] << " tz: " << t[2] << std::endl;
  std::cout << "tx: " << t_[0] << " ty: " << t_[1] << " tz: " << t_[2] << std::endl;
  */
  
  /*
  
  double ypr_[3];
  toYPR(p_.q, ypr_);
  
  Eigen::MatrixXd J_ypr(6, 7);
  J_ypr.setZero();
  
  J_ypr.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  
  toYPRJacobian(q, J_ypr.block<3, 4>(3, 3));
  
  C = J_ypr*C*J_ypr.transpose();
  
  // Rad to deg
  ypr_[0] /= K; ypr_[1] /= K; ypr_[2] /= K;
  
  Eigen::MatrixXd J_deg = (1.0/K)*Eigen::Matrix3d::Identity();
  C.block<3, 3>(3, 3) = J_deg*C.block<3, 3>(3, 3)*J_deg.transpose();
  
  std::cout << "tx: " << t[0] << "; ty: " << t[1] << "; tz: " << t[2] << std::endl;
  std::cout << "tx: " << p_.t[0] << "; ty: " << p_.t[1] << "; tz: " << p_.t[2] << std::endl;
  
  std::cout << "yaw: " << ypr[0]/K << "; pitch: " << ypr[1]/K << "; roll: " << ypr[2]/K << std::endl;
  std::cout << "yaw: " << ypr_[0] << "; pitch: " << ypr_[1] << "; roll: " << ypr_[2] << std::endl;
  
  std::cout << "C: (" << C.rows() << ", " << C.cols() << ")" << std::endl;
  std::cout << C << std::endl;
  std::cout << std::endl;
  
  */
  
  return 0;
}

/*
int main() {

  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
  PRNG = new std::mt19937(seed);

  std::cout << std::fixed << std::setprecision(6);
  std::cout << "Jacobian test" << std::endl;
  
  double ypr[3] = {RandomReal(-180.0, 180.0), RandomReal(-180.0, 180.0), RandomReal(-180.0, 180.0)};
  
  std::cout << "y = " << ypr[0] << "; p = " << ypr[1] << "; r = " << ypr[2] << ";" << std::endl;
  
  const double s = 4.0;
  Eigen::MatrixXd C = s*Eigen::Matrix3d::Identity();
  
  // Deg to rad
  ypr[0] *= K; ypr[1] *= K; ypr[2] *= K;
  
  Eigen::MatrixXd J_rad = K*Eigen::Matrix3d::Identity();
  C = J_rad*C*J_rad.transpose();
  
  double q[4];
  fromYPR(ypr, q);
  
  Eigen::MatrixXd J_q;
  fromYPRJacobian(ypr, J_q);
  C = J_q*C*J_q.transpose();
  
  double ypr_[3];
  toYPR(q, ypr_);
  
  Eigen::MatrixXd J_ypr;
  toYPRJacobian(q, J_ypr);
  C = J_ypr*C*J_ypr.transpose();
  
  ypr_[0] /= K; ypr_[1] /= K; ypr_[2] /= K;
  
  Eigen::MatrixXd J_deg = (1.0/K)*Eigen::Matrix3d::Identity();
  C = J_deg*C*J_deg.transpose();
  
  std::cout << "y = " << ypr_[0] << "; p = " << ypr_[1] << "; r = " << ypr_[2] << ";" << std::endl;
  
  std::cout << "C:" << std::endl;
  std::cout << C << std::endl;
  std::cout << std::endl;
  
  return 0;
}
*/