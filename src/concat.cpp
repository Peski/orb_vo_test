
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "SE3Quat.h"

inline std::istream& operator>>(std::istream &lhs, SE3Quat &rhs) {

    lhs >> rhs.t[0] >> rhs.t[1] >> rhs.t[2] >> rhs.q[1] >> rhs.q[2] >> rhs.q[3] >> rhs.q[0];
    rhs.q_normalize(); // to handle finite precision
    return lhs;
}

inline void read_file(const std::string &path, std::vector<double>& timestamps, std::vector<SE3Quat>& poses ) {

    timestamps.clear();
    poses.clear();

    std::ifstream input(path);
    if (!input.is_open()) return;

    for (std::string line; std::getline(input, line);) {
        if (line.empty() || line.front() == '#') continue;

        std::istringstream iss(line);
        double timestamp;
        SE3Quat pose;
        if (iss >> timestamp >> pose) {
          timestamps.push_back(std::move(timestamp));
          poses.push_back(std::move(pose));
        }
    }
}

std::string to_string(double number) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(6) << number;
  return ss.str();
}

int main(int argc, char* argv[]) {

  if (argc != 2) {
    std::cerr << "Usage: concat <input>" << std::endl;
    return 1;
  }

  std::vector<double> timestamps;
  std::vector<SE3Quat> poses;
  read_file(argv[1], timestamps, poses);
  
  Eigen::Matrix3d R;
  R <<  0.0, -1.0,  0.0,
        1.0,  0.0,  0.0,
        0.0,  0.0,  1.0;
        
  Eigen::Isometry3d Tbc_ = Eigen::Isometry3d::Identity();
  Tbc_.linear() = R;
  
  SE3Quat Tbc(Tbc_);
  
  SE3Quat current_pose;
  for (int i = 0; i < timestamps.size(); ++i) {
    const double& timestamp = timestamps.at(i);
    SE3Quat relative_pose = poses.at(i);
    
    // Scale
    //relative_pose.t[0] /= 800.0;
    //relative_pose.t[1] /= 800.0;
    //relative_pose.t[2] /= 800.0;
    
    relative_pose = Tbc.inverse() * relative_pose * Tbc;
    current_pose = current_pose * relative_pose;
    
    SE3Quat pose = current_pose;
    
    std::cout << to_string(timestamp) << " "
            << to_string(pose.t[0]) << " "
            << to_string(pose.t[1]) << " "
            << to_string(pose.t[2]) << " "
            << to_string(pose.q[1]) << " "
            << to_string(pose.q[2]) << " "
            << to_string(pose.q[3]) << " "
            << to_string(pose.q[0]) << " "
            << std::endl;
  }

  return 0;
}