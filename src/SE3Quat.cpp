
#include "SE3Quat.h"

#include <cmath>

SE3Quat::SE3Quat()
  : t{0.0, 0.0, 0.0}, q{1.0, 0.0, 0.0, 0.0}
{
}

SE3Quat::SE3Quat(double tx, double ty, double tz, double qw, double qx, double qy, double qz)
  : t{tx, ty, tz}, q{qw, qx, qy, qz}
{
    q_normalize();
}

SE3Quat::SE3Quat(const Eigen::Vector3d& t_vec, const Eigen::Vector4d& q_vec)
  : t{t_vec(0), t_vec(1), t_vec(2)}, q{q_vec(0), q_vec(1), q_vec(2), q_vec(3)}
{
    q_normalize();
}

template<typename Scalar, int Type>
SE3Quat::SE3Quat(const Eigen::Transform<Scalar, 3, Type> &T)
{
    Eigen::Quaternion<Scalar> quat(T.rotation());

    t[0] = T.translation().x();
    t[1] = T.translation().y();
    t[2] = T.translation().z();
    q[0] = quat.w();
    q[1] = quat.x();
    q[2] = quat.y();
    q[3] = quat.z();
    
    q_normalize();
}

// To Eigen Transform (implicit conversion)
template<typename Scalar, int Type>
SE3Quat::operator Eigen::Transform<Scalar, 3, Type>() const {
    Eigen::Quaternion<Scalar> quat(q[0], q[1], q[2], q[3]);
    quat.normalize();

    Eigen::Transform<Scalar, 3, Type> T(quat);

    T.translation().x() = t[0];
    T.translation().y() = t[1];
    T.translation().z() = t[2];

    return T;
}

// Inverse
SE3Quat SE3Quat::inverse() const
{
    return Eigen::Isometry3d(*this).inverse();
}

// Concatenation
SE3Quat SE3Quat::operator *(const SE3Quat& other) const
{
    return Eigen::Isometry3d(*this) * Eigen::Isometry3d(other);
}

void SE3Quat::q_normalize()
{
    qw_positive();

    const double norm = 1.0 / std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
    q[0] *= norm;
    q[1] *= norm;
    q[2] *= norm;
    q[3] *= norm;
}

void SE3Quat::qw_positive()
{
    if (q[0] < 0.0)
    {
        q[0] *= -1.0;
        q[1] *= -1.0;
        q[2] *= -1.0;
        q[3] *= -1.0;
    } 
}

SE3Quat SE3Quat::Identity()
{
    return SE3Quat(0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
}

void SE3Quat::fromYPR(const double ypr[3], double q[4])
{
  const double cy = std::cos(0.5*ypr[0]), sy = std::sin(0.5*ypr[0]);
	const double cp = std::cos(0.5*ypr[1]), sp = std::sin(0.5*ypr[1]);
	const double cr = std::cos(0.5*ypr[2]), sr = std::sin(0.5*ypr[2]);
	
	const double ccc = cr*cp*cy;
	const double ccs = cr*cp*sy;
	const double css = cr*sp*sy;
	const double sss = sr*sp*sy;
	const double scc = sr*cp*cy;
	const double ssc = sr*sp*cy;
	const double csc = cr*sp*cy;
	const double scs = sr*cp*sy;
	
	q[0] = ccc + sss;
	q[1] = scc - css;
	q[2] = csc + scs;
	q[3] = ccs - ssc;
}
