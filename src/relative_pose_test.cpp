
// STL
#include <chrono>
#include <cmath>
#include <iomanip>
#include <ios>
#include <iostream>
#include <random>
#include <vector>

// Eigen
#include <Eigen/Core>

#include "jacobians.hpp"
#include "SE3Quat.h"

const double K = EIGEN_PI / 180.0;

std::mt19937* PRNG;

void fromYPR(const double ypr[3], double q[4])
{
  const double cy = std::cos(0.5*ypr[0]), sy = std::sin(0.5*ypr[0]);
	const double cp = std::cos(0.5*ypr[1]), sp = std::sin(0.5*ypr[1]);
	const double cr = std::cos(0.5*ypr[2]), sr = std::sin(0.5*ypr[2]);
	
	const double ccc = cr*cp*cy;
	const double ccs = cr*cp*sy;
	const double css = cr*sp*sy;
	const double sss = sr*sp*sy;
	const double scc = sr*cp*cy;
	const double ssc = sr*sp*cy;
	const double csc = cr*sp*cy;
	const double scs = sr*cp*sy;
	
	q[0] = ccc + sss;
	q[1] = scc - css;
	q[2] = csc + scs;
	q[3] = ccs - ssc;
}

void toYPR(const double q[4], double ypr[3]) {
  const double d = q[0]*q[2] - q[1]*q[3];
  
  if (d > 0.49999) {
    ypr[0] = -2.0*std::atan2(q[1], q[0]);
    ypr[1] = 0.5 * EIGEN_PI;
    ypr[2] = 0.0;
  } else if (d < -0.49999) {
    ypr[0] = 2.0*std::atan2(q[1], q[0]);
    ypr[1] = -0.5 * EIGEN_PI;
    ypr[2] = 0.0;
  } else { // General case
    ypr[0] = std::atan2(2.0*(q[0]*q[3] + q[1]*q[2]), 1.0 - 2.0*(q[2]*q[2] + q[3]*q[3]));
    ypr[1] = std::asin(2.0*d);
    ypr[2] = std::atan2(2.0*(q[0]*q[1] + q[2]*q[3]), 1.0 - 2.0*(q[1]*q[1] + q[2]*q[2]));
  }
}

double qnorm(const double q[4]) {
  return std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
}

void qnormalize(double q[4]) {
  
  double norm = (q[0] < 0.0) ? -1.0 : 1.0;
  norm /= std::sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
  
  q[0] *= norm;
  q[1] *= norm;
  q[2] *= norm;
  q[3] *= norm;
}

void qinverse(const double q[4], double qinv[4]) {
  qinv[0] =  q[0];
  qinv[1] = -q[1];
  qinv[2] = -q[2];
  qinv[3] = -q[3];
}

void qproduct(const double p[4], const double q[4], double pq[4]) {
  pq[0] = p[0]*q[0] - p[1]*q[1] - p[2]*q[2] - p[3]*q[3];
  pq[1] = p[0]*q[1] + p[1]*q[0] + p[2]*q[3] - p[3]*q[2];
  pq[2] = p[0]*q[2] - p[1]*q[3] + p[2]*q[0] + p[3]*q[1];
  pq[3] = p[0]*q[3] + p[1]*q[2] - p[2]*q[1] + p[3]*q[0];
}

void qrotate(const double q[4], const double v[3], double result[3]) {
  const double t2 =  q[0] * q[1];
  const double t3 =  q[0] * q[2];
  const double t4 =  q[0] * q[3];
  const double t5 = -q[1] * q[1];
  const double t6 =  q[1] * q[2];
  const double t7 =  q[1] * q[3];
  const double t8 = -q[2] * q[2];
  const double t9 =  q[2] * q[3];
  const double t1 = -q[3] * q[3];
  
  result[0] = 2.0 * ((t8 + t1) * v[0] + (t6 - t4) * v[1] + (t3 + t7) * v[2]) + v[0];
  result[1] = 2.0 * ((t4 + t6) * v[0] + (t5 + t1) * v[1] + (t9 - t2) * v[2]) + v[1];
  result[2] = 2.0 * ((t7 - t3) * v[0] + (t2 + t9) * v[1] + (t5 + t8) * v[2]) + v[2];
}

double RandomGaussian(const double mean, const double stddev) {
  std::normal_distribution<double> distribution(mean, stddev);
  return distribution(*PRNG);
}

double RandomReal(const double min, const double max) {
  std::uniform_real_distribution<double> distribution(min, max);
  return distribution(*PRNG);
}

int main() {

  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
  PRNG = new std::mt19937(seed);

  std::cout << std::fixed << std::setprecision(6);
  std::cout << "Jacobian test" << std::endl;
  
  double ypr_1[3] = {RandomReal(-180.0, 180.0), RandomReal(-90.0, 90.0), RandomReal(-180.0, 180.0)};
  double t_1[3] = {RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0)};
  
  double ypr_2[3] = {RandomReal(-180.0, 180.0), RandomReal(-90.0, 90.0), RandomReal(-180.0, 180.0)};
  double t_2[3] = {RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0), RandomReal(-10.0, 10.0)};
  
  ypr_1[0] *= K; ypr_1[1] *= K; ypr_1[2] *= K;
  ypr_2[0] *= K; ypr_2[1] *= K; ypr_2[2] *= K;
  
  double q_1[4];
  fromYPR(ypr_1, q_1);
  
  double q_2[4];
  fromYPR(ypr_2, q_2);
  
  double q_1_inv[4];
  qinverse(q_1, q_1_inv);
  
  double dq[4];
  qproduct(q_1_inv, q_2, dq);
  
  qnormalize(dq);
  
  double dt_[3];
  dt_[0] = t_2[0] - t_1[0];
  dt_[1] = t_2[1] - t_1[1];
  dt_[2] = t_2[2] - t_1[2];
  
  double dt[3];
  qrotate(q_1_inv, dt_, dt);
  
  SE3Quat p_1;
  p_1.q[0] = q_1[0];
  p_1.q[1] = q_1[1];
  p_1.q[2] = q_1[2];
  p_1.q[3] = q_1[3];
  
  p_1.t[0] = t_1[0];
  p_1.t[1] = t_1[1];
  p_1.t[2] = t_1[2];
  
  SE3Quat p_2;
  p_2.q[0] = q_2[0];
  p_2.q[1] = q_2[1];
  p_2.q[2] = q_2[2];
  p_2.q[3] = q_2[3];
  
  p_2.t[0] = t_2[0];
  p_2.t[1] = t_2[1];
  p_2.t[2] = t_2[2];
  
  SE3Quat dp = p_1.inverse() * p_2;
  
  std::cout << std::fixed << std::setprecision(6);
  
  std::cout << "qw: " << dq[0] << " qx: " << dq[1] << " qy: " << dq[2] << " qz: " << dq[3] << std::endl;
  std::cout << "qw: " << dp.q[0] << " qx: " << dp.q[1] << " qy: " << dp.q[2] << " qz: " << dp.q[3] << std::endl;
  
  std::cout << "tx: " << dt[0] << " ty: " << dt[1] << " tz: " << dt[2] << std::endl;
  std::cout << "tx: " << dp.t[0] << " ty: " << dp.t[1] << " tz: " << dp.t[2] << std::endl;
  
  return 0;
}
