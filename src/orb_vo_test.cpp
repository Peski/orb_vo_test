
#include <algorithm>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <string>
#include <vector>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// ROS
#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <geographic_msgs/GeoPoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

#include "gps.hpp"
#include "SE3Quat.h"

#include "quat.hpp"
#include "jacobians.hpp"

const double K = 0.1;

bool initialized = false;
std::vector<double> gps_timestamps;
std::vector<geographic_msgs::GeoPose> gps_poses;

ros::Publisher pub_imu;

std::int64_t src_idx = -1, dst_idx = -1;

std::mt19937* PRNG;

double RandomGaussian(const double mean, const double stddev) {
  std::normal_distribution<double> distribution(mean, stddev);
  return distribution(*PRNG);
}

void sample_pose(const SE3Quat& p, SE3Quat& result, Eigen::MatrixXd& covariance) {
  SE3Quat e;
  
  double t[3] = {RandomGaussian(0.0, K*0.05), RandomGaussian(0.0, K*0.05), RandomGaussian(0.0, K*0.05)};
  double ypr[3] = {RandomGaussian(0.0, K*0.3), RandomGaussian(0.0, K*0.3), RandomGaussian(0.0, K*0.3)};
  
  const double C = EIGEN_PI / 180.0;
  
  covariance.resize(6, 6);
  covariance.setZero();
  
  covariance.block<3, 3>(0, 0) = std::pow(K*0.1, 2)*Eigen::Matrix3d::Identity();
  covariance.block<3, 3>(3, 3) = std::pow(K*0.5, 2)*Eigen::Matrix3d::Identity();
  
  ypr[0] *= C;
  ypr[1] *= C;
  ypr[2] *= C;
  
  Eigen::MatrixXd J;  
  J.resize(6, 6);
  J.setZero();
  
  J.block<3, 3>(0, 0) =     Eigen::Matrix3d::Identity();
  J.block<3, 3>(3, 3) = C * Eigen::Matrix3d::Identity();
  
  covariance = J*covariance*J.transpose();
  
  double q[4];
  fromYPR(ypr, q);
  
  e.q[0] = q[0];
  e.q[1] = q[1];
  e.q[2] = q[2];
  e.q[3] = q[3];
  
  e.t[0] = t[0];
  e.t[1] = t[1];
  e.t[2] = t[2];
  
  J.resize(7, 6);
  J.setZero();
  
  J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  fromYPRJacobian(ypr, J.block<4, 3>(3, 3));
  
  covariance = J*covariance*J.transpose();
  
  Eigen::MatrixXd J_p(7, 7);
  
  J.resize(7, 7);
  compositionJacobians(p, e, J_p, J);
  
  covariance = J*covariance*J.transpose();
  
  result = p*e;
  
  J.resize(6, 7);
  J.setZero();
  
  J.block<3, 3>(0, 0) = Eigen::Matrix3d::Identity();
  toYPRJacobian(result.q, J.block<3, 4>(3, 3));
  
  covariance = J*covariance*J.transpose();
}

std::int64_t find_closest(const std::vector<double>& v, double value) {
  if (v.size() < 2) return -1;
  
  std::vector<double>::const_iterator nxt = std::lower_bound(v.cbegin(), v.cend(), value);
  if (nxt == v.cbegin() || nxt == v.cend()) return -1;
  
  std::vector<double>::const_iterator prv = std::prev(nxt);
  if ((value - *prv) < (*nxt - value)) {
    return std::distance(v.cbegin(), prv);
  } else {
    return std::distance(v.cbegin(), nxt);
  }
}

std::string to_string(double number) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(6) << number;
  return ss.str();
}

void GPSCallback(const geographic_msgs::GeoPoseStampedPtr& msg) {
  gps_timestamps.push_back(msg->header.stamp.toSec());
  gps_poses.push_back(msg->pose);
}

void SyncCallback(const std_msgs::HeaderPtr& msg) {

  src_idx = dst_idx;
  dst_idx = find_closest(gps_timestamps, msg->stamp.toSec());
  
  if (!initialized) {
    initialized = true;
    return;
  }
  
  if (src_idx == -1 || dst_idx == -1) {
    std::cerr << "[SyncCallback] Invalid index!" << std::endl;
    return;
  }
  
  geographic_msgs::GeoPose src_geo = gps_poses.at(src_idx);
  geographic_msgs::GeoPose dst_geo = gps_poses.at(dst_idx);
  
  Eigen::Isometry3d src_ecef = Eigen::Isometry3d::Identity();
  src_ecef.translation() = gps::geodetic2Ecef(src_geo.position.latitude, src_geo.position.longitude, src_geo.position.altitude);
  src_ecef.linear() = gps::nRe(src_geo.position.latitude, src_geo.position.longitude).transpose() * Eigen::Quaterniond(src_geo.orientation.w, src_geo.orientation.x, src_geo.orientation.y, src_geo.orientation.z).toRotationMatrix();
  
  Eigen::Isometry3d dst_ecef = Eigen::Isometry3d::Identity();
  dst_ecef.translation() = gps::geodetic2Ecef(dst_geo.position.latitude, dst_geo.position.longitude, dst_geo.position.altitude);
  dst_ecef.linear() = gps::nRe(dst_geo.position.latitude, dst_geo.position.longitude).transpose() * Eigen::Quaterniond(dst_geo.orientation.w, dst_geo.orientation.x, dst_geo.orientation.y, dst_geo.orientation.z).toRotationMatrix();
  
  SE3Quat relative_pose(src_ecef.inverse() * dst_ecef);
  
  // Noise
  SE3Quat observed;
  Eigen::MatrixXd covariance(6, 6);
  sample_pose(relative_pose, observed, covariance);
  relative_pose = observed;
  
  geometry_msgs::PoseWithCovarianceStamped imu_data;
  
  imu_data.header.stamp = msg->stamp;
  
  imu_data.pose.pose.position.x = relative_pose.t[0];
  imu_data.pose.pose.position.y = relative_pose.t[1];
  imu_data.pose.pose.position.z = relative_pose.t[2];
  
  imu_data.pose.pose.orientation.x = relative_pose.q[1];
  imu_data.pose.pose.orientation.y = relative_pose.q[2];
  imu_data.pose.pose.orientation.z = relative_pose.q[3];
  imu_data.pose.pose.orientation.w = relative_pose.q[0];
  
  for (int i = 0; i < 6; ++i)
    for (int j = 0; j < 6; ++j)
      imu_data.pose.covariance[i*6 + j] = covariance(i, j);
  
  std::cout << to_string(msg->stamp.toSec()) << " "
            << to_string(relative_pose.t[0]) << " "
            << to_string(relative_pose.t[1]) << " "
            << to_string(relative_pose.t[2]) << " "
            << to_string(relative_pose.q[1]) << " "
            << to_string(relative_pose.q[2]) << " "
            << to_string(relative_pose.q[3]) << " "
            << to_string(relative_pose.q[0]) << " "
            << std::endl;
  
  /*
  std::cout << "Covariance: " << std::endl;
  std::cout << covariance << std::endl;
  std::cout << std::endl;
  */
  
  pub_imu.publish(imu_data);
}

int main(int argc, char* argv[]) {

  unsigned seed = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
  PRNG = new std::mt19937(seed);

  ros::init(argc, argv, "orb_vo_test");
  
  std::cout << "orb_vo_test (K=" << K << ")" << std::endl;
  
  ros::NodeHandle nodeHandler;
  
  ros::Subscriber sub_gps = nodeHandler.subscribe("/ins/reference", 100, &GPSCallback);
  ros::Subscriber sub_sync = nodeHandler.subscribe("/orb_vo/sync", 100, &SyncCallback);
  
  pub_imu = nodeHandler.advertise<geometry_msgs::PoseWithCovarianceStamped>("/imu/estimate", 100);
  
  ros::spin();
  
  return 0;
}
