
#ifndef SE3QUAT_H
#define SE3QUAT_H

#include <Eigen/Core>
#include <Eigen/Geometry>

// SE3 pose with quaternion representation for rotation
//   This structure is used for optimization for ceres and within INS
struct SE3Quat {
    // Translation (x, y, z order) and quaternion (w, x, y, z order)
    double t[3], q[4];

    // Default constructor (identity)
    SE3Quat();

    // Full constructor
    SE3Quat(double tx, double ty, double tz, double qw, double qx, double qy, double qz);

    // t_vec: (tx, ty, tz); q_vec: (qw, qx, qy, qz)
    SE3Quat(const Eigen::Vector3d& t_vec, const Eigen::Vector4d& q_vec);

    // From Eigen Transform (constructor)
    template<typename Scalar, int Type>
    SE3Quat(const Eigen::Transform<Scalar, 3, Type> &T);

    // To Eigen Transform (implicit conversion)
    template<typename Scalar, int Type>
    operator Eigen::Transform<Scalar, 3, Type>() const;

    // Inverse operator
    SE3Quat inverse() const;

    // Concatenation (this * other)
    SE3Quat operator *(const SE3Quat& other) const;

    // Quaternion normalization (to avoid numerical issues)
    void q_normalize();
    
    // qw >= 0 convention (to avoid ambiguity)
    void qw_positive();

    // Identity transformation
    static SE3Quat Identity();
    
    // Construct Quaternion (w, x, y, z order) from yaw-pitch-roll angles (in radians)
    static void fromYPR(const double ypr[3], double q[4]);
};

#endif // SE3QUAT_H