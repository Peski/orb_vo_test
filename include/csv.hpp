
#ifndef CSV_HPP_
#define CSV_HPP_

// STL
#include <algorithm>
#include <cstddef>
#include <fstream>
#include <iomanip>
#include <stdexcept>
#include <string>
#include <vector>

// Eigen
#include <Eigen/Core>

#include "util/macros.h"

namespace csv {

template<typename Scalar, int Options = Eigen::ColMajor>
Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Options> read (const std::string &path, char delim = ',') {

    std::string line;
    std::ifstream input(path);

    std::size_t cols = 0;
    std::vector<std::vector<Scalar>> data;
    while (std::getline(input, line)) {
        if (line.empty()) continue;
        if (line.front() == '#') continue;

        std::vector<Scalar> data_row;
        data_row.reserve(cols);

        std::string value;
        std::stringstream line_stream(line);
        while (std::getline(line_stream, value, delim)) {
            if (value.empty()) continue;
            Scalar numeric_value;
            bool is_numeric = true;
            try { numeric_value = static_cast<Scalar>(std::stod(value)); }
            catch (const std::invalid_argument&) { is_numeric = false; }
            RUNTIME_ASSERT(is_numeric == true);
            data_row.push_back(numeric_value);
        }

        cols = std::max(cols, data_row.size());
        if (!data_row.empty()) data.push_back(data_row);
    }

    if (input.fail() && !input.eof())
        return Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>::Zero(0, 0);

    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Options> matrix(data.size(), cols);
    for (std::size_t i = 0; i < data.size(); ++i) {
        std::vector<Scalar>& data_row = data.at(i);
        RUNTIME_ASSERT(data_row.size() == cols);
        matrix.row(i) = Eigen::Map<Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>(data_row.data(), 1, cols);
    }

    return matrix;
}

template<typename Derived>
bool write(const Eigen::MatrixBase<Derived> &data, std::ostream& output, int precision = 6, std::string delim = ",") {

    Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, delim, "\n");

    output << std::setprecision(precision);
    output << std::fixed << data.format(fmt) << std::endl;

    return (!output.fail() && !output.bad());
}

template<typename Derived>
bool write(const Eigen::MatrixBase<Derived> &data, const std::string &path, int precision = 6, std::string delim = ",") {

    std::ofstream output(path);

    return write(data, output, precision, delim);
}

} // namespace csv

#endif // CSV_HPP_
